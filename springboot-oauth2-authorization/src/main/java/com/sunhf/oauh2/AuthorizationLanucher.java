package com.sunhf.oauh2;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

@SpringBootApplication
@EnableAuthorizationServer
public class AuthorizationLanucher {

    public static void main(String[] args) {
        new SpringApplicationBuilder(AuthorizationLanucher.class)
                .run(args);
    }
}
