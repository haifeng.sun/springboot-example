package com.sunhf.rocketmq2;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MQProducer;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.apache.rocketmq.common.message.Message;

import javax.annotation.PostConstruct;

@Component
public class SyncProducer {

    @Autowired
    private MQProducer producer;

    @PostConstruct
    public void init() {
        if(producer != null) {
            System.out.println("生产者已启动");

            Message message = new Message();
            message.setTopic("springboot-starter-test");
            message.setTags("springboot");
            message.setBody("这是一条测试消息".getBytes());
            try {
                producer.send(message);
            } catch (MQClientException e) {
                e.printStackTrace();
            } catch (RemotingException e) {
                e.printStackTrace();
            } catch (MQBrokerException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("消息发送成功");
        } else {
            System.out.println("生产者未启动");
        }

    }

}
