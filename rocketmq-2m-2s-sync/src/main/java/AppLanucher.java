import com.sunhf.rocketmq.annotation.EnableRocketMQ;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan("com.sunhf.rocketmq2")
@EnableRocketMQ
public class AppLanucher {

    public static void main(String[] args) {
        SpringApplication.run(AppLanucher.class, args);
    }
}
