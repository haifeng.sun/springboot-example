package com.sunhf.oauth2;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
@EnableResourceServer
public class ResourceLanucher {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ResourceLanucher.class)
                .run(args);
    }
}
