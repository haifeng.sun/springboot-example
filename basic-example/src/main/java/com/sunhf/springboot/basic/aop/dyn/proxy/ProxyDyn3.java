package com.sunhf.springboot.basic.aop.dyn.proxy;

/**
 * 嵌套调用层级关系
 */
public class ProxyDyn3 implements DynProxy3 {
    public void a() {
        System.out.println("a");
    }

    public void b() {
        System.out.println("b");
    }

    public void ab() {
        a();
        b();
    }
}
