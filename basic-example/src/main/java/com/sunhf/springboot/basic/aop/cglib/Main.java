package com.sunhf.springboot.basic.aop.cglib;

import net.sf.cglib.core.DebuggingClassWriter;
import net.sf.cglib.proxy.Enhancer;

public class Main {

    public static void main(String[] args) {
        //设置将cglib生成的代理类字节码生成到指定位置
        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "/Users/sunhf/Documents/MegaDrive/AppData/idea/demo/design-model/target/classes");
        Enhancer e = new Enhancer();
        e.setSuperclass(CGLibDemo.class);
        e.setCallback(new Interceptor());
        CGLibDemo cg = (CGLibDemo)e.create();
        cg.hi();
        System.out.println(cg.toString());
    }
}
