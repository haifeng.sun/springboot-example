package com.sunhf.springboot.basic.aop.dyn.proxy;

public interface DynProxy2 {

    void rumMeBy2(String message);

    void runMe2By2(String message);
}
