#动态代理实现

##描述
类的生命周期：class文件(可于硬盘/内存中存储，字节码文件可以在内存中生成)->class对象（由classloader加载）->实例化->卸载<br/>
class对象也可以被卸载，通常发生在fullgc。
由代理类控制对象访问，代理类负责为委托类进行一些额外的业务处理。

##应用场景
1. 在闭源代码上附加统一的增强业务逻辑
2. 收集日志或进行某些预处理
3. 统一异常处理
4. 可应用于延迟加载


##流程
实现InvocationHandler->(ProxyInvoker）=> 创建原对象接口(DynProxy, DynProxy2) => 实现原对象(Proxy1,Proxy2,Proxy1By2) =>创建动态代理类->(ProxyFactory)=>执行方法

##相关API
####Proxy
```
// 方法 1: 该方法用于获取指定代理对象所关联的调用处理器
static InvocationHandler getInvocationHandler(Object proxy) 
 
// 方法 2：该方法用于获取关联于指定类装载器和一组接口的动态代理类的类对象
static Class getProxyClass(ClassLoader loader, Class[] interfaces) 
 
// 方法 3：该方法用于判断指定类对象是否是一个动态代理类
static boolean isProxyClass(Class cl) 
 
// 方法 4：该方法用于为指定类装载器、一组接口及调用处理器生成动态代理类实例
static Object newProxyInstance(ClassLoader loader, Class[] interfaces, 
    InvocationHandler h)
    

```
####InvocationHandler
```
// 该方法负责集中处理动态代理类上的所有方法调用。第一个参数既是代理类实例，第二个参数是被调用的方法对象
// 第三个方法是调用参数。调用处理器根据这三个参数进行预处理或分派到委托类实例上发射执行
Object invoke(Object proxy, Method method, Object[] args)    
```

##源码解析
###运行原理
通过classloader 将生成的代理类装载后进行使用。


##注意
1. 在创建代理对象后，强制转换只能转换为原对象的接口。在实现了多接口的场景下，只能转换为其中一个接口。
2. 对代理对象的所有接口方法调用都会转发到InvocationHandler.invoke()
3. 动态代理可以理解为代购服务。即在要执行的方法上附加额外的逻辑。


##问题
1.接口中并不是所有的方法都需要增强的内容，如何实现对特定的方法进行代理。
答：JDK提供的动态代理局限于接口级，不支持方法级切面。如需更灵活的功能，请考虑cglib。

2.如何查看内存中生成的代理类对象
答：通过dump中的工具包将内存中的代理类直接写到文件然后反编译查看。

3.多个接口的情况下，是否会生成多个代理类
答：不会生成多个类。


4.接口中有3个方法 a, b, ab 其中ab方法体内调用了a, b 方法 这3个方法都进行了动态代理，那么 ab方法中调用的a,b是否也会被添加增强代码
答：动态代理只会对被代理对象的第一层入口有效果，不会影响到ab中调用的内容。invoke方法是在运行时添加的

5.InvocationHandler中有proxy对象，是否可以调用其中的方法？
答：不能进行调用，调用该proxy方法后会递归触发invoke，导致无限递归。

6.Object类中的方法如equals、tostring等是否也会被加上增强逻辑。
答：会转发equals、hashcode、tostring这3个方法

##优缺点
###优点
1. 隐藏委托类的实现，调用者只需要和代理类进行交互
2. 解偶，在不改变委托累代码的情况下做一些额外处理，比如添加初始化判断等操作。
###缺点
1. 无法摆脱interface的限制，因为所有代理类的父类都是Proxy，所以无法再进行继承。
2. 无法对class进行动态代理，因为多继承在java本质上就行不通



##官方描述

###Proxy
1. Proxy提供了创建动态代理类和实例的静态方法，它也是由这些方法创建的所有动态代理类的超类。
####代理类具有以下属性：

1. 代理类是公共的，最终的，而不是抽象的，如果所有代理接口都是公共的。
2. 如果任何代理接口是非公开的，代理类是非公开的，最终的，而不是抽象的 。
3. 代理类的不合格名称未指定。 然而，以字符串"$Proxy"开头的类名空间应该保留给代理类。
4. 一个代理类扩展了java.lang.reflect.Proxy 。
5. 代理类完全按照相同的顺序实现其创建时指定的接口。
6. 如果一个代理类实现一个非公共接口，那么它将被定义在与该接口相同的包中。 否则，代理类的包也是未指定的。 请注意，程序包密封不会阻止在运行时在特定程序包中成功定义代理类，并且类也不会由同一类加载器定义，并且与特定签名者具有相同的包。
7. 由于代理类实现了在其创建时指定的所有接口， getInterfaces在其类对象上调用getInterfaces将返回一个包含相同列表接口的数组（按其创建时指定的顺序），在其类对象上调用getMethods将返回一个数组的方法对象，其中包括这些接口中的所有方法，并调用getMethod将在代理接口中找到可以预期的方法。
8. Proxy.isProxyClass方法将返回true，如果它通过代理类 - 由Proxy.getProxyClass返回的类或由Proxy.newProxyInstance返回的对象的类 - 否则为false。
9. 所述java.security.ProtectionDomain代理类的是相同由引导类装载程序装载系统类，如java.lang.Object ，因为是由受信任的系统代码生成代理类的代码。 此保护域通常将被授予java.security.AllPermission 。
10. 每个代理类有一个公共构造一个参数，该接口的实现InvocationHandler ，设置调用处理程序的代理实例。 而不必使用反射API来访问公共构造函数，也可以通过调用Proxy.newProxyInstance方法来创建代理实例，该方法将调用Proxy.getProxyClass的操作与调用处理程序一起调用构造函数。

####代理实例具有以下属

1. 给定代理实例proxy和其代理类Foo ，以下表达式将返回true：
   proxy instanceof Foo 
2. 并且以下演员操作将会成功（而不是投掷一个ClassCastException ）：
   (Foo) proxy 
3. 每个代理实例都有一个关联的调用处理程序，它被传递给它的构造函数。 静态Proxy.getInvocationHandler方法将返回与作为其参数传递的代理实例关联的调用处理程序。
4. 代理实例上的接口方法调用将被编码并分派到调用处理程序的invoke方法，如该方法的文档所述。
5. hashCode ， equals在代理实例上的toString中声明的java.lang.Object或toString或toString或toString方法将被编码并分派到调用处理程序的invoke方法，方法与接口方法调用被编码和调度相同。 传递给invoke的方法对象的声明类将为java.lang.Object 。 从java.lang.Object的代理实例的其他公共方法不会被代理类覆盖，因此这些方法的调用与java.lang.Object 。

####多代理接口中复制的方
原文
>当代理类的两个或多个接口包含具有相同名称和参数签名的方法时，代理类接口的顺序变得重要。 当在代理实例上调用这种重复方法时，传递给调用处理程序的方法对象不一定是其声明类可以通过调用代理方法的接口的引用类型进行分配的对象。 存在此限制，因为生成的代理类中的相应方法实现无法确定其调用的接口。 因此，当在代理实例上调用重复的方法时，代理类的方法列表中包含方法（直接或通过超级接口继承）的最重要的接口中的方法的Method对象被传递给调用处理程序的invoke方法，而不管方法调用发生的引用类型。
><br/>还要注意，当将一个重复的方法分派到调用处理程序时， invoke方法可能只会将可分配给所有可以调用的所有代理接口中的方法的throws子句中的一种异常类型的检查异常类型抛出通过。 如果invoke方法抛出经过检查的异常是不能分配给任何通过它可以通过调用代理接口中的方法声明的异常类型，那么选中UndeclaredThrowableException将通过代理实例调用抛出。 此限制意味着，并非所有通过调用返回的异常类型getExceptionTypes上方法传递给对象invoke方法一定可以成功地抛出invoke方法。
###InvocationHandler
>处理代理实例上的方法调用并返回结果。 当在与之关联的代理实例上调用方法时，将在调用处理程序中调用此方法。
 <br/>参数
 <br/>proxy - 调用该方法的代理实例
 <br/>method -所述方法对应于调用代理实例上的接口方法的实例。 方法对象的声明类将是该方法声明的接口，它可以是代理类继承该方法的代理接口的超级接口。
 <br/>args -包含的方法调用传递代理实例的参数值的对象的阵列，或null如果接口方法没有参数。 原始类型的参数包含在适当的原始包装器类的实例中，例如java.lang.Integer或java.lang.Boolean 。
 <br/>结果
 <br/>从代理实例上的方法调用返回的值。 如果接口方法的声明返回类型是原始类型，则此方法返回的值必须是对应的基本包装类的实例; 否则，它必须是可声明返回类型的类型。 如果此方法返回的值是null和接口方法的返回类型是基本类型，那么NullPointerException将由代理实例的方法调用抛出。 如上所述，如果此方法返回的值，否则不会与接口方法的声明的返回类型兼容，一个ClassCastException将代理实例的方法调用将抛出。
 <br/>异常
 <br/>Throwable - 从代理实例上的方法调用抛出的异常。 异常类型必须可以分配给接口方法的throws子句中声明的任何异常类型java.lang.RuntimeException检查的异常类型java.lang.RuntimeException或java.lang.Error 。 如果检查的异常是由这种方法是不分配给任何的中声明的异常类型throws接口方法的子句，则一个UndeclaredThrowableException包含有由该方法抛出的异常将通过在方法调用抛出代理实例。
##动态代理类文件样例(从内存中导出，参见dump包下的工具类)
```

package com.sun.proxy;

import com.sunhf.design.proxy.dyn.proxy.DynProxy3;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.UndeclaredThrowableException;

public final class $Proxy0 extends Proxy implements DynProxy3 {
    private static Method m1;
    private static Method m3;
    private static Method m4;
    private static Method m2;
    private static Method m0;
    private static Method m5;

    public $Proxy0(InvocationHandler var1) throws  {
        super(var1);
    }

    public final boolean equals(Object var1) throws  {
        try {
            return (Boolean)super.h.invoke(this, m1, new Object[]{var1});
        } catch (RuntimeException | Error var3) {
            throw var3;
        } catch (Throwable var4) {
            throw new UndeclaredThrowableException(var4);
        }
    }

    public final void b() throws  {
        try {
            super.h.invoke(this, m3, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    public final void a() throws  {
        try {
            super.h.invoke(this, m4, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    public final String toString() throws  {
        try {
            return (String)super.h.invoke(this, m2, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    public final int hashCode() throws  {
        try {
            return (Integer)super.h.invoke(this, m0, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    public final void ab() throws  {
        try {
            super.h.invoke(this, m5, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    static {
        try {
            m1 = Class.forName("java.lang.Object").getMethod("equals", Class.forName("java.lang.Object"));
            m3 = Class.forName("com.sunhf.design.proxy.dyn.proxy.DynProxy3").getMethod("b");
            m4 = Class.forName("com.sunhf.design.proxy.dyn.proxy.DynProxy3").getMethod("a");
            m2 = Class.forName("java.lang.Object").getMethod("toString");
            m0 = Class.forName("java.lang.Object").getMethod("hashCode");
            m5 = Class.forName("com.sunhf.design.proxy.dyn.proxy.DynProxy3").getMethod("ab");
        } catch (NoSuchMethodException var2) {
            throw new NoSuchMethodError(var2.getMessage());
        } catch (ClassNotFoundException var3) {
            throw new NoClassDefFoundError(var3.getMessage());
        }
    }
}
```
