package com.sunhf.springboot.basic.aop.dyn.proxy;

public interface DynProxy {

    void rumMe(String message);
}
