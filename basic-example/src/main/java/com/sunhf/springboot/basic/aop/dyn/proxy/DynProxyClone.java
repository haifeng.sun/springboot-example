package com.sunhf.springboot.basic.aop.dyn.proxy;

public interface DynProxyClone {

    void rumMe(String message);
}
