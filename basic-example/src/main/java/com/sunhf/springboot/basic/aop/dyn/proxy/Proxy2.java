package com.sunhf.springboot.basic.aop.dyn.proxy;

public class Proxy2 implements DynProxy {

    public void rumMe(String message) {
        System.out.println("proxy 2 >>>" + message);
    }
}
