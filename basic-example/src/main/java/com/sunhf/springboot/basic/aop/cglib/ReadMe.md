#CGLib
#特点
1. cglib针对类实现代理，主要针对指定的类生成代理子类，覆盖需要被代理的方法。
2. 被代理类不能是final的，被代理中的方法可以是final，但该方法不会被代理。
3. 与JDK动态代理不同的是，JDK动态代理只能针对实现了接口的类生成代理，而不能针对类
4. JDK动态代理，实现类即便是final的也可以被正常代理。
5. 它不是基于反射机制的，cglib通过java字节码生成动态代理对象，依赖asm库，它实际上继承了被代理对象。实际上它更像一个拦截器
6. tostring equals hashcode也会被代理
7. 由于cglib运行期没有用反射，所以执行效率上比JDK快。而在对象创建上，cglib是通过被代理对象的class文件进行代理类的生成，所以创建代理过程没有JDK快。

#代理类解析
###xxFastClassxxx
1. 它继承了FastClass，他有两个方法getIndex和Invoke。在getIndex方法中对Test的每个方法建立索引，并根据入参（方法名+方法描述）返回对应的索引。
Invoke根据指定的索引，以ol为入参调用对象O的方法，这样就避免了反射调用，提高了效率。
##双亲委派
1. ClassLoader.loadClass(String name, boolean resolve)先检查是否被加载过，若没有则调用父类的classloader，父类为空则用本类的classloader
2. 加载顺序 父类static块 子类static块，父类资源，子类资源
3. 因为启动时有检查，所以与rt.jar同名的类是不会被加载执行的。但是编译正常。
