package com.sunhf.springboot.basic.aop.dyn;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 代理类执行器
 */
public class ProxyInvoker implements InvocationHandler {

    private Object intf;

    /**
     * 注册代理接口
     * @param intf
     */
    public void register(Object intf) {
        this.intf = intf;
    }


    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object o = method.invoke(intf, args);
        after();
        return o;
    }

    private void before(){
        System.out.println("before");
    }


    private void after() {
        System.out.println("after");
    }

}
