package com.sunhf.springboot.basic.aop.dyn.proxy;

public class ProxyMulti1 implements DynProxy,DynProxy2 {
    public void rumMe(String message) {
        System.out.println("ProxyMulti1 rumMe >>>"+ message);
    }

    public void rumMeBy2(String message) {
        System.out.println("ProxyMulti1 rumMeBy2 >>>"+ message);
    }

    public void runMe2By2(String message) {
        System.out.println("ProxyMulti1 runMe2By2 >>>"+ message);
    }
}
