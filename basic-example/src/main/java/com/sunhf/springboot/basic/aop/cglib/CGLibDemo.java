package com.sunhf.springboot.basic.aop.cglib;

/**
 * CGLib动态代理
 */
//不能设置为final
public  class CGLibDemo {

    public final void hi(){
        System.out.println("hi");
    }
}
