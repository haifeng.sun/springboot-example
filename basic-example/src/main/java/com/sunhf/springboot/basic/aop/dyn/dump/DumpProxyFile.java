package com.sunhf.springboot.basic.aop.dyn.dump;



import com.sunhf.springboot.basic.aop.dyn.ProxyFactory;
import com.sunhf.springboot.basic.aop.dyn.proxy.ProxyMulti1;
import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 将内存中生成的代理类写到文件中
 */
public class DumpProxyFile {

    /**
     * 将代理类对象写入到文件
     * @param clazz 代理类对象
     */
    public static void write(Class clazz) {
        //根据类信息和提供的代理类名称，生成字节码
        byte[] classFile = ProxyGenerator.generateProxyClass(clazz.getName(), clazz.getInterfaces());
        String paths = DumpProxyFile.class.getResource(".").getPath();
        System.out.println(paths);
        FileOutputStream out = null;

        try {
            //保留到硬盘中
            out = new FileOutputStream(paths + clazz.getName() + ".class");
            out.write(classFile);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Object obj =  ProxyFactory.createProxyInstance(new ProxyMulti1());
        write(obj.getClass());
    }
}
