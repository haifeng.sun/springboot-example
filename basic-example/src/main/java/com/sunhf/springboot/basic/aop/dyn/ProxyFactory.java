package com.sunhf.springboot.basic.aop.dyn;

import java.lang.reflect.Proxy;

/**
 * 动态代理委托类
 */
public class ProxyFactory  {

    private static ProxyInvoker INVOKER = new ProxyInvoker();

    /**
     * 创建动态代理类
     * @return 返回的是动态代理类 返回值只能转换为代理对象的接口类
     */
    public static Object createProxyInstance(Object instance) {
        return createProxyInstance(instance, instance.getClass().getInterfaces());
    }


    /**
     * 创建动态代理类
     * @return 返回的是动态代理类 返回值只能转换为代理对象的接口类
     */
    public static Object createProxyInstance(Object instance, Class<?>[] interfaces) {
        INVOKER.register(instance); //注册新对象
        /**
         * 参数1：被代理对象
         * 参数2：被代理对象实现的接口集合（即指定可以转换成的接口，前提是原对象必须已实现该接口）
         * 参数3：代理对象
         */
        Object intance = Proxy.newProxyInstance(instance.getClass().getClassLoader(), interfaces, INVOKER);
        return intance;
    }




}
