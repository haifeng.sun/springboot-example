package com.sunhf.springboot.basic.aop.dyn;


import com.sunhf.springboot.basic.aop.dyn.proxy.*;

public class Main {

    public static void main(String[] args) {
        /* 设置此系统属性,让JVM生成的Proxy类写入文件 */
        System.setProperty("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        DynProxy p1 = (DynProxy) ProxyFactory.createProxyInstance(new Proxy1());
        p1.rumMe("hi");


        DynProxy p2 = (DynProxy) ProxyFactory.createProxyInstance(new Proxy2());
        p2.rumMe("hello");

        //检查equals等方法是否也会被代理
        System.out.println(">>>>>equals:" + p1.equals(p2));

        //检查toString 是否被代理
        System.out.println(">>>>>toString:" + p1.toString());


        DynProxy2 dp2= (DynProxy2) ProxyFactory.createProxyInstance(new Proxy1By2());
        dp2.rumMeBy2("are you ok ?");
        dp2.runMe2By2("i'm ok.");

        //多接口实现
        ProxyMulti1 p = new ProxyMulti1();
        Object multiObj = ProxyFactory.createProxyInstance(p);
        DynProxy pm1 = (DynProxy) multiObj;
        pm1.rumMe("i'm ruoji?");

        DynProxy2 pm2 = (DynProxy2) multiObj;
        pm2.rumMeBy2("i'm dalao?");
        pm2.runMe2By2("i'm tuhao");

        //指定实现接口
        DynProxy pm3 = (DynProxy) ProxyFactory.createProxyInstance(p, new Class[]{DynProxy.class});
        pm3.rumMe("i'm 3");
        DynProxy2 pm4 = (DynProxy2) ProxyFactory.createProxyInstance(p, new Class[]{DynProxy2.class});
        pm4.rumMeBy2("i'm 3");
        pm4.runMe2By2("i'm 4");

        //方法嵌套
        DynProxy3 dp3 = (DynProxy3) ProxyFactory.createProxyInstance(new ProxyDyn3());
        dp3.a();
        dp3.b();
        dp3.ab();

        //两个相同方法的接口
        Object snp = ProxyFactory.createProxyInstance(new SameNameProxy());
        DynProxy dsnp = (DynProxy) snp;
        dsnp.rumMe("dsnp");
        DynProxyClone dsnpc = (DynProxyClone) snp;
        dsnpc.rumMe("dsnpc");

    }

}
