package com.sunhf.springboot.basic.aop.stat;

public class Main {

    public static void main(String[] args) {
        StaticProxy p1 = new Proxy1();
        StaticProxy p2 = new Proxy2();
        p1.runMe();
        p2.runMe();
    }
}
