package com.sunhf.springboot.basic.aop.dyn.proxy;

public final class Proxy1 implements DynProxy {

    public void rumMe(String message) {
        System.out.println("proxy 1 >>>" + message);
    }
}
