package com.sunhf.springboot.basic.aop.stat;

/**
 * 代理类1
 */
public class Proxy1 implements StaticProxy {


    public void invoke(String message) {
        System.out.println("this is :" + message);
    }

    public void runMe() {
        before();
        invoke("proxy1");
        after();

    }



    private void before(){
        System.out.println("before");
    }

    private void after() {
        System.out.println("after");
    }
}
