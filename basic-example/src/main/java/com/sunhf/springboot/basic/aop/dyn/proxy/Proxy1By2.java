package com.sunhf.springboot.basic.aop.dyn.proxy;

public class Proxy1By2 implements  DynProxy2 {
    public void rumMeBy2(String message) {
        System.out.println("Proxy1By2 >>>>"+message);
    }

    public void runMe2By2(String message) {
        System.out.println("Proxy1By2 runMe2 >>>>"+message);
    }
}
