package com.sunhf.springboot.basic.aop.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/*
动态代理增强实现类
 */
public class Interceptor implements MethodInterceptor {

    private void before(){
        System.out.println("before");
    }

    private void after() {
        System.out.println("after");
    }



    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        before();
        Object obj = methodProxy.invokeSuper(o, objects);
        after();
        return obj;
    }
}
