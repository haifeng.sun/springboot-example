package com.sunhf.springboot.basic.aop.aop;

import java.util.Map;
import java.util.WeakHashMap;

public class ATest {

    public static void main(String[] args) throws InterruptedException {
        Map<String, String> m = new WeakHashMap<String, String>();
        String a = "1";
        m.put(a, "!@3");
        a = null;
        System.gc();

        System.out.println(m.isEmpty());
        Thread.sleep(1000);
        System.out.println(m.isEmpty());
    }
}
