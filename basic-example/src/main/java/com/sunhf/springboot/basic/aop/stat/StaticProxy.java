package com.sunhf.springboot.basic.aop.stat;

/**
 * 静态代理接口
 */
public interface StaticProxy {


    void runMe();
}
