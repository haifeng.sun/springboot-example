package com.sunhf.springboot.basic.aop.dyn.proxy;

public interface DynProxy3 {

    void a();

    void b();

    void ab();
}
