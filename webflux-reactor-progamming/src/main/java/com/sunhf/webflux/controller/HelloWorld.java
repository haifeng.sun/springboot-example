package com.sunhf.webflux.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class HelloWorld {
    @GetMapping("/hi")
    public Mono<String> hi() {
        return Mono.just("Hello World");
    }
}
