package com.flux.subscriber;

/**
 * 订阅
 */
public interface Subscription {
    void request(long n);
    void cancel();
}
