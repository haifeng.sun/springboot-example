package com.flux.subscriber;

/**
 * 订阅者接口
 * @param <T>
 */
public interface Subscriber<T> {

    /**
     * 订阅
     * @param s
     */
    void onSubscribe(Subscription s);

    /**
     * 下一个元素
     * @param t
     */
    void onNext(T t);

    /**
     * 出错处理
     * @param t
     */
    void onError(Throwable t);

    /**
     * 善后
     */
    void onComplete();

}
