package com.flux.subscriber;

public class FluxArray<T> extends Flux<T> {

    private T[] array;

    public FluxArray(T[] array) {
        this.array = array;
    }
    @Override
    void subscribe(Subscriber<? super T> s) {
        s.onSubscribe(new ArraySubscription<>(s, array));
    }

    class ArraySubscription<T> implements  Subscription {

        Subscriber<? super T> actual;
        T[] array;
        int index;
        boolean canceled;

        public ArraySubscription(Subscriber<? super T> actual, T[] array) {
            this.actual = actual;
            this.array = array;
        }


        @Override
        public void request(long n) {
            if(canceled) return;
            long length = array.length;
            for(int i = 0; i < n && index < length;i ++) {
                actual.onNext(array[index++]);
            }
            if(index == length) {
                actual.onComplete();
            }
        }

        @Override
        public void cancel() {
            this.canceled = true;
        }
    }
}
