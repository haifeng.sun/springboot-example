package com.flux.subscriber;

import org.junit.Test;

public class MainTest {

    @Test
    public void test(){
        Flux.just(1,2,3,4,5).subscribe(new Subscriber<Integer>() {
            @Override
            public void onSubscribe(Subscription s) {
                System.out.println("onSubscribe");
                s.request(7); //订阅时请求6个元素
            }

            @Override
            public void onNext(Integer integer) {
                System.out.println("onNext:"+integer);
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("onError:");
            }

            @Override
            public void onComplete() {
                System.out.println("onComplete");
            }
        });
    }
}
