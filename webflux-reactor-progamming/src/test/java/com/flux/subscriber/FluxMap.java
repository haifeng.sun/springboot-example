package com.flux.subscriber;

import java.util.function.Function;

public class FluxMap<T, R> extends Flux<R> {

    Flux<? extends T> source;
    Function<? super T, ? extends R> mapper;

    public FluxMap(Flux<? extends T> source, Function<? super T, ? extends R> mapper) {
        this.source = source;
        this.mapper = mapper;
    }

    @Override
    void subscribe(Subscriber<? super R> s) {

    }
}
