package com.flux.subscriber;

import java.util.function.Function;

public abstract class Flux<T> implements Publisher<T> {

    abstract void subscribe(Subscriber<? super T> s);

    public static <T> Flux<T> just(T... data) {
        return new FluxArray<>(data);
    }

    public <V> Flux<V> map(Function<? super T, ? extends V> mapper) {
        return new FluxMap<>(this, mapper);
    }
}
