#Webflux演示
##描述
异步模型是大势所趋，不能再拖了。。。
基于Reactive Streams
##参考资料
http://www.spring4all.com/article/1025
https://www.jianshu.com/p/f4ff6d74ad4a
[精]https://www.ibm.com/developerworks/cn/java/spring5-webflux-reactive/index.html
反应式技术栈解析http://www.infoq.com/cn/articles/Servlet-and-Reactive-Stacks-Spring-Framework-5
【精】官方译文https://springcloud.cc/web-reactive.html

demo https://github.com/hantsy/spring-reactive-sample#spring-data-redis

mono和flux解释https://www.jianshu.com/p/40a0ebe321be

【中文】反应式编程概览https://github.com/ZhongyangMA/webflux-streaming-demo/wiki/%E5%8F%8D%E5%BA%94%E5%BC%8F%E7%BC%96%E7%A8%8B%E6%A6%82%E8%A7%88%EF%BC%88%E4%B8%AD%E6%96%87%E7%89%88%EF%BC%89