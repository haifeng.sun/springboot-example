package com.sunhf.transaction.service;

import java.util.Map;

public class OrderServiceMock implements OrderService {

    public Long createOrder(Map<String, Object> params) {
        return Long.MIN_VALUE;
    }

    public Boolean closeOrder(Map<String, Object> params) {
        return null;
    }

    public Boolean isExpireOrder(Map<String, Object> params) {
        return null;
    }
}
