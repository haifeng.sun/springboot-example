package com.sunhf.transaction.service;

import java.util.Map;

/**
 * 订单生成服务
 */
public interface OrderService {

     /**
      * 生成订单
      * @param params
      * @return
      */
    Long createOrder(Map<String, Object> params);

    /**
     * 关闭订单
     * @param params
     * @return
     */
    Boolean closeOrder(Map<String, Object> params);

    /**
     * 检查订单是否到期
     * @param params
     * @return
     */
    Boolean isExpireOrder(Map<String, Object> params);

}
