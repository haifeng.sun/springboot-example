package com.sunhf.transaction.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.sunhf.rocketmq.enums.DelayTimeLevel;
import com.sunhf.transaction.service.OrderService;
import org.apache.rocketmq.client.producer.MQProducer;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.Random;

@Service(
        version = "1.0.0",
        application = "${dubbo.application.id}",
        protocol = "${dubbo.protocol.id}",
        registry = "${dubbo.registry.id}",
        mock = "true",
        actives = 2
)
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private MQProducer producer;

    @Override
    public Long createOrder(Map<String, Object> params) {
        if(params == null || params.isEmpty()) throw new RuntimeException("非法的参数");
        logger.info("假装做了一些逻辑处理,生成了订单,请求参数:"+ JSON.toJSONString(params));
        try {
            Thread.sleep(new Random().nextInt(100)); //模拟延迟
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Message m = new Message();
        m.setBody("延迟消息5秒钟测试".getBytes());
        m.setTopic("springboot-starter-test");
        m.setDelayTimeLevel(DelayTimeLevel.SECOND_5.getLevel());
        m.setTags("test-single");
        try {
            producer.send(m, new OrderCallBack());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Random().nextLong();
    }

    @Override
    public Boolean closeOrder(Map<String, Object> params) {
        return null;
    }

    @Override
    public Boolean isExpireOrder(Map<String, Object> params) {
        return null;
    }


}
