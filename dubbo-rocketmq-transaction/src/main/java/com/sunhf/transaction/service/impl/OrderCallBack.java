package com.sunhf.transaction.service.impl;

import com.alibaba.fastjson.JSON;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;

public class OrderCallBack implements SendCallback {
    @Override
    public void onSuccess(SendResult sendResult) {
        System.out.println("消息发送成功: result:"+ JSON.toJSONString(sendResult));
    }

    @Override
    public void onException(Throwable e) {
        e.printStackTrace();
    }
}
