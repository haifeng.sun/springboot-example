package com.sunhf;

import com.sunhf.rocketmq.annotation.EnableRocketMQ;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.sunhf.transaction.service.impl")
@EnableRocketMQ(consumerInit = false)
public class DubboLanucher {

    public static void main(String[] args) {
        new SpringApplicationBuilder(DubboLanucher.class)
                .web(WebApplicationType.NONE)
                .run(args);

    }
}
