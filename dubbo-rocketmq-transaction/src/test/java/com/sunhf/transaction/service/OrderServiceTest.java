package com.sunhf.transaction.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sunhf.DubboLanucher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DubboLanucher.class)
public class OrderServiceTest {
    //
    //绕过zk可直接在reference中指定url url = "dubbo://192.168.2.10:12345"
    @Reference(version = "${demo.service.version}",
            application = "${dubbo.application.id}")
    private OrderService demoService;

    @Test
    public void test(){
        Map<String, Object> map = new HashMap<>();
        map.put("1","1");
        System.out.println("\n"+demoService.createOrder(map));
    }
    //限流测试
    @Test
    public void limitTest() {
        ExecutorService service = Executors.newCachedThreadPool();
        int N = 1000;
        CyclicBarrier barrier  = new CyclicBarrier(100);
        for(int i=0;i<N;i++)
            service.execute(new OrderThread(barrier, demoService));

    }

    class OrderThread extends Thread{

        OrderService o;
        CyclicBarrier cyclicBarrier;


        public OrderThread(CyclicBarrier cyclicBarrier, OrderService o) {
            this.o = o;
            this.cyclicBarrier = cyclicBarrier;
        }

        @Override
        public void run() {
            try {
                cyclicBarrier.await();
                Map<String, Object> map = new HashMap<>();
                map.put("1","1");
                System.out.println(o.createOrder(map));
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }
}
