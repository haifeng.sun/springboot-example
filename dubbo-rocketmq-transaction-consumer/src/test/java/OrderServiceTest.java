import com.alibaba.dubbo.config.annotation.Reference;
import com.sunhf.transaction.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RocketMQLanucher.class)
public class OrderServiceTest {
    //
    //绕过zk可直接在reference中指定url url = "dubbo://192.168.2.10:12345"
    @Reference(version = "${demo.service.version}",
            application = "${dubbo.application.id}")
    private OrderService demoService;

    @Test
    public void test(){
        Map<String, Object> map = new HashMap<>();
        map.put("1","1");
        System.out.println("\n"+demoService.createOrder(map));
    }
}
