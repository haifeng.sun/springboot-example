package com.sunhf.transaction.entity;

import java.io.Serializable;


/**
 * 实体
 * 表名 CONSUMER_MESSAGE_LOG
 *
 * @author sunhf
 * @date 2018-07-29 20:31:26
 */
public class ConsumerMessageLogEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    private Long id;
    /**
     * RocketMQ MessageId
     */
    private String messageId;
    /**
     * 0 成功 1 失败 2未消费
     */
    private Integer status;

    /**
     * 设置：
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置：RocketMQ MessageId
     */
    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    /**
     * 获取：RocketMQ MessageId
     */
    public String getMessageId() {
        return messageId;
    }
    /**
     * 设置：0 成功 1 失败 2未消费
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取：0 成功 1 失败 2未消费
     */
    public Integer getStatus() {
        return status;
    }
}
