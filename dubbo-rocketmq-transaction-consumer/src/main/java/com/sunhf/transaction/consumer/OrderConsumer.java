package com.sunhf.transaction.consumer;

import com.alibaba.fastjson.JSON;
import com.sunhf.rocketmq.annotation.Consumer;
import com.sunhf.rocketmq.base.AbstractMQPushConsumer;
import com.sunhf.rocketmq.base.MessageExtConst;
import com.sunhf.transaction.entity.ConsumerMessageLogEntity;
import com.sunhf.transaction.service.ConsumerMessageLogService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

@Consumer(consumerGroup = "DefaultGroup", topic = "springboot-starter-test", tag = "*")
public class OrderConsumer extends AbstractMQPushConsumer<String> {

    @Autowired
    private ConsumerMessageLogService messageLogService;

    @Override
    public boolean process(String s, Map<String, Object> map) {
        Object messageId = map.get(MessageExtConst.PROPERTY_EXT_MSG_ID);
        if(messageId == null) return false;
        try{
            Map<String, Object> param = new HashMap<>();
            param.put("messageId", messageId);
            param.put("status", 0);
            int total = messageLogService.queryTotal(param);
            if(total > 0) {
                return true; //幂等性
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(s);
        System.out.println(JSON.toJSONString(map));
        ConsumerMessageLogEntity e = new ConsumerMessageLogEntity();
        e.setMessageId(messageId.toString());
        e.setStatus(0);
        messageLogService.save(e);
        return true;
    }
}
