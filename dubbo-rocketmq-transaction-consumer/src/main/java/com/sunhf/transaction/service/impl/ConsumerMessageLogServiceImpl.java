package com.sunhf.transaction.service.impl;


import com.sunhf.transaction.common.SnowflakeIdWorker;
import com.sunhf.transaction.dao.ConsumerMessageLogDao;
import com.sunhf.transaction.entity.ConsumerMessageLogEntity;
import com.sunhf.transaction.service.ConsumerMessageLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Service实现类
 *
 * @author sunhf
 * @date 2018-07-29 20:31:26
 */
@Service("consumerMessageLogService")
public class ConsumerMessageLogServiceImpl implements ConsumerMessageLogService {

    @Autowired
    private ConsumerMessageLogDao consumerMessageLogDao;

    @Autowired
    private SnowflakeIdWorker idWorker;

    @Override
    public ConsumerMessageLogEntity queryObject(Long id) {
        return consumerMessageLogDao.queryObject(id);
    }

    @Override
    public List<ConsumerMessageLogEntity> queryList(Map<String, Object> map) {
        return consumerMessageLogDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return consumerMessageLogDao.queryTotal(map);
    }

    @Override
    public int save(ConsumerMessageLogEntity consumerMessageLog) {
        consumerMessageLog.setId(idWorker.nextId());
        return consumerMessageLogDao.save(consumerMessageLog);
    }

    @Override
    public int update(ConsumerMessageLogEntity consumerMessageLog) {
        return consumerMessageLogDao.update(consumerMessageLog);
    }

    @Override
    public int delete(Long id) {
        return consumerMessageLogDao.delete(id);
    }

    @Override
    public int deleteBatch(Long[]ids) {
        return consumerMessageLogDao.deleteBatch(ids);
    }
}
