package com.sunhf.transaction.dao;


import com.sunhf.transaction.entity.ConsumerMessageLogEntity;

/**
 * Dao
 *
 * @author sunhf
 * @date 2018-07-29 20:31:26
 */
public interface ConsumerMessageLogDao extends BaseDao<ConsumerMessageLogEntity> {

}
