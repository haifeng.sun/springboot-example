import com.sunhf.rocketmq.annotation.EnableRocketMQ;
import com.sunhf.transaction.common.SnowflakeIdWorker;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.sunhf.transaction")
@EnableRocketMQ(producerInit = false)
@MapperScan(basePackages = {"**.mapper","**.dao"})
public class RocketMQLanucher {



    public static void main(String[] args) {
        new SpringApplicationBuilder(RocketMQLanucher.class)
                .web(WebApplicationType.NONE)
                .run(args);

    }

    @Bean
    public SnowflakeIdWorker snowflakeIdWorker(){
        return new SnowflakeIdWorker(1,2);
    }


}