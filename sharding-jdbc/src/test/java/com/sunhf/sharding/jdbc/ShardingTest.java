package com.sunhf.sharding.jdbc;


import com.sunhf.JdbcLanucher;
import com.sunhf.sharding.jdbc.dao.UserDao;
import com.sunhf.sharding.jdbc.entity.UserEntity;
import io.shardingsphere.core.keygen.DefaultKeyGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JdbcLanucher.class)
public class ShardingTest {


    @Autowired
    public UserDao userDao;

    @Autowired
    private DefaultKeyGenerator keyGenerator;

    @Test
    public void test(){

    }

    @Test
    public void testQueryUser() {
        Map<String, Object> param = new HashMap<>();
        param.put("id",-1497366528);
        UserEntity e  = userDao.queryOne(param);
        System.out.println(e);
    }

    @Test
    public void testInsertUserEntity() {
        UserEntity entity = new UserEntity();
        entity.setId(keyGenerator.generateKey().longValue());
        entity.setEmail("test@qq.com");
        entity.setMobile("15011508000");
        entity.setStatus("1");
        Date date = new Date();
        entity.setCreatetime(date);
        entity.setUpdatetime(date);
        userDao.save(entity);
    }
}
