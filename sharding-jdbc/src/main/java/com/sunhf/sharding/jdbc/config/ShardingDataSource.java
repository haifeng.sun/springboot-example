package com.sunhf.sharding.jdbc.config;

import com.alibaba.druid.pool.DruidDataSource;
import io.shardingsphere.core.api.ShardingDataSourceFactory;
import io.shardingsphere.core.api.config.ShardingRuleConfiguration;
import io.shardingsphere.core.api.config.TableRuleConfiguration;
import io.shardingsphere.core.api.config.strategy.InlineShardingStrategyConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;
import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 分库分表数据源配置
 */
//@Configuration
public class ShardingDataSource {


      @Bean
      public DataSource dataSource2() {
          // 配置真实数据源
          Map<String, DataSource> dataSourceMap = new HashMap<>();

          // 配置第一个数据源
          DruidDataSource dataSource1 = new DruidDataSource();
          dataSource1.setDriverClassName("com.mysql.cj.jdbc.Driver");
          dataSource1.setUrl("jdbc:mysql://192.168.2.203:3307/split-db0?useUnicode=true&characterEncoding=utf8&autoReconnect=true&failOverReadOnly=false");
          dataSource1.setUsername("root");
          dataSource1.setPassword("root");
          dataSourceMap.put("ds0", dataSource1);

          // 配置第二个数据源
          DruidDataSource dataSource2 = new DruidDataSource();
          dataSource2.setDriverClassName("com.mysql.cj.jdbc.Driver");
          dataSource2.setUrl("jdbc:mysql://192.168.2.203:3307/split-db1?useUnicode=true&characterEncoding=utf8&autoReconnect=true&failOverReadOnly=false");
          dataSource2.setUsername("root");
          dataSource2.setPassword("root");
          dataSourceMap.put("ds1", dataSource2);

          // 配置Order表规则
          TableRuleConfiguration orderTableRuleConfig = new TableRuleConfiguration();
          orderTableRuleConfig.setLogicTable("SHARING_USER");
          orderTableRuleConfig.setActualDataNodes("ds${0..1}.SHARING_USER_${0..1}");

          // 配置分库 + 分表策略
          orderTableRuleConfig.setDatabaseShardingStrategyConfig(new InlineShardingStrategyConfiguration("id", "ds${id % 2}"));
          orderTableRuleConfig.setTableShardingStrategyConfig(new InlineShardingStrategyConfiguration("id", "SHARING_USER_${id % 2}"));

          // 配置分片规则
          ShardingRuleConfiguration shardingRuleConfig = new ShardingRuleConfiguration();
          shardingRuleConfig.getTableRuleConfigs().add(orderTableRuleConfig);

          // 省略配置order_item表规则...
          // ...

          // 获取数据源对象
          try {
              DataSource dataSource = ShardingDataSourceFactory.createDataSource(dataSourceMap, shardingRuleConfig, new ConcurrentHashMap(), new Properties());
              return dataSource;
          } catch (SQLException e) {
              e.printStackTrace();
          }
          return null;
      }
}
