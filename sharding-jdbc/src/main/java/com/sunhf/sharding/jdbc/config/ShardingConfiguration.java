package com.sunhf.sharding.jdbc.config;

import io.shardingsphere.core.api.yaml.YamlShardingDataSourceFactory;
import io.shardingsphere.core.jdbc.core.datasource.ShardingDataSource;
import io.shardingsphere.core.keygen.DefaultKeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

@Configuration
public class ShardingConfiguration {

    @Bean
    public DefaultKeyGenerator defaultKeyGenerator(){
        return new DefaultKeyGenerator();
    }

    @Bean
    public DataSource shardingDataSource() throws IOException, SQLException {
        return YamlShardingDataSourceFactory.createDataSource(new File(new ClassPathResource("sharding-jdbc.yml").getURI()));

    }

}
