package com.sunhf.sharding.jdbc.dao;

import com.sunhf.sharding.jdbc.entity.UserEntity;

import java.util.List;
import java.util.Map;

public interface UserDao {


    void save(UserEntity userEntity);


    void update(UserEntity entity);


    void delete(UserEntity entity);


    UserEntity queryOne(Map<String, Object> param);

    List<UserEntity> queryList(Map<String, Object> param);
}
