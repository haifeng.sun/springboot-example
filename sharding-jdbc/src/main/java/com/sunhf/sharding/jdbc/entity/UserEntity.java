package com.sunhf.sharding.jdbc.entity;

import lombok.Data;

import java.util.Date;

@Data
public class UserEntity {

    private Long id;
    private String username;
    private String mobile;
    private String email;
    private String status;
    private Date createtime;
    private Date updatetime;


}
