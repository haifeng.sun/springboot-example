package com.sunhf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.sunhf.sharding.jdbc")
@MapperScan(basePackages = {"**.dao"})
public class JdbcLanucher {

    public static void main(String[] args) {
        new SpringApplicationBuilder(JdbcLanucher.class)
//                .web(WebApplicationType.NONE)
                .run(args);

    }
}
